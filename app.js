'use strict';

// Required modules
let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let compression = require('compression');
let favicon = require('serve-favicon');
let app = express();
let node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
let config = require('./server/config.json')[node_env];

/*
// Connect to MySQL
let mysql = require('mysql');
let mysqlConnection = mysql.createConnection({
    host     : config['mysql_db_host'],
    user     : config['mysql_db_user'],
    password : config['mysql_db_pass'],
    database : config['mysql_db_name']
});
mysqlConnection.connect();
console.log(mysqlConnection);
*/

// CORS Config
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	next();
});

// add compression
app.use(compression());

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Remove headers
app.disable('x-powered-by');

// All Routes here
let index = require('./server/routes/index');
app.use('/', index);

// Send all other requests to the Angular app
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// connect to server
let server = app.listen(process.env.PORT || config.app_port, function () {
    console.log("App listening at http://%s:%s", server.address().address, server.address().port);
});

module.exports = app;