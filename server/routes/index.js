'use strict';

// Required modules
let express = require('express');
let router = express.Router();
let mysqlConn = require('../db-connect/mysqlConn');

// routes
router.use('/', mysqlConn, require('./battlesController'));

module.exports = router;