'use strict';

// Required modules
let express = require('express');
let router = express.Router();
let mysqlFnUtil = require('../utils/mysqlUtil');
let TABLE_NAME = 'battles';
let TABLE_COLUMNS = [ 'name',
	'year',
	'battle_number',
	'attacker_king',
	'defender_king',
	'attacker_1',
	'attacker_2',
	'attacker_3',
	'attacker_4',
	'defender_1',
	'defender_2',
	'defender_3',
	'defender_4',
	'attacker_outcome',
	'battle_type',
	'major_death',
	'major_capture',
	'attacker_size',
	'defender_size',
	'attacker_commander',
	'defender_commander',
	'summer',
	'location',
	'region',
	'note' ];

// Base route
router.get('/', function (req, res, next) {
	res.send('Testing Route !!!');
});

//
// Get battles places where battle happened
router.get('/list', function (req, res, next) {
    console.log('\n---------------------');
    console.log('API Call Start --> ', req.originalUrl);

    let mysqlConnection = req.mysqlConnection;
    let mysqlQuery = 'SELECT DISTINCT(location) FROM '+TABLE_NAME+' WHERE location != ""';
    mysqlFnUtil.poolQuery(mysqlConnection, mysqlQuery)
        .then(function (placesListRaw) {        	
            // not found
            if (!placesListRaw || placesListRaw.length === 0) {
            	console.log('Battle places not found in DB');
                return res.send({'error': 1, 'msg': 'Battle places not found', 'data': []});
            }

            // Success
            let placesList = new Array();
            let placesListRawCnt = placesListRaw.length;
            for(let i = 0; i < placesListRawCnt; i++) {
            	placesList.push(placesListRaw[i]['location']);
            }            
            return res.send({'error': 0, 'msg': 'Battle places list', 'data': placesList});
        })
        .catch(function (errObject) {
            console.log('In catch block --> API - ', req.originalUrl);
            console.log(errObject);
			console.log('\n-------------------');
			return res.send({'error': 1, 'msg': 'Battle places not found. Something wen\'t wrong. Please try again later.', 'data': []});
        });
});// EOF api

//
// Get total battle count
router.get('/count', function (req, res, next) {
    console.log('\n---------------------');
    console.log('API Call Start --> ', req.originalUrl);

    let mysqlConnection = req.mysqlConnection;
    let mysqlQuery = 'SELECT COUNT(*) as cnt FROM '+TABLE_NAME;
    mysqlFnUtil.poolQuery(mysqlConnection, mysqlQuery)
        .then(function (battleCount) {        	
            // not found
            if (!battleCount || battleCount.length === 0) {
            	console.log('No battles found in DB');
                return res.send({'error': 1, 'msg': 'No battles found in DB', 'count': 0});
            }

            // Success
            return res.send({'error': 0, 'msg': 'Battle count', 'count': battleCount[0]['cnt']});
        })
        .catch(function (errObject) {
            console.log('In catch block --> API - ', req.originalUrl);
            console.log(errObject);
			console.log('\n-------------------');
			return res.send({'error': 1, 'msg': 'No battles found in DB. Something wen\'t wrong. Please try again later.', 'count': 0});
        });
});// EOF api

//
// Search api
router.get('/search', function (req, res, next) {
    console.log('\n---------------------');
    console.log('API Call Start --> ', req.originalUrl);
    console.log(req.query);

    // Get query parameters
    let queryParamObj = req.query;
    let queryParamObjCnt = Object.keys(queryParamObj).length;
    if(queryParamObjCnt == 0) {
    	return res.send({'error': 1, 'msg': 'Please search for Something', 'data': [], 'count': 0});
    }

    // loop to form search condition
    let QUERY_TYPE = 'AND';
    if(queryParamObj.hasOwnProperty('query_type') && (queryParamObj['query_type'].toLowerCase() === 'or')) {
    	QUERY_TYPE = 'OR';
    }
    let whereCondition = '';
    let forCounter = 0;
    for(let i in queryParamObj) {
    	forCounter++;
    	if(i.toLowerCase() === 'query_type') {
    		continue;
    	}

    	// check if table column present
    	if(TABLE_COLUMNS.indexOf(i.toLowerCase()) === -1) {
    		continue;
    	}

    	whereCondition += ' AND '+i+'="'+queryParamObj[i]+'"';
    }

    if(!whereCondition) {
    	return res.send({'error': 1, 'msg': 'Please search for Something', 'data': [], 'count': 0});
    }

    let mysqlConnection = req.mysqlConnection;
    let mysqlQuery = 'SELECT * FROM '+TABLE_NAME+' WHERE 1=1 '+whereCondition;
    mysqlFnUtil.poolQuery(mysqlConnection, mysqlQuery)
        .then(function (battlesList) {
            // not found
            if (!battlesList || battlesList.length === 0) {
            	console.log('Battle list not found in DB');
                return res.send({'error': 1, 'msg': 'Battle list not found', 'data': [], 'count': 0});
            }

            // Success  
            return res.send({'error': 0, 'msg': 'Battle list', 'data': battlesList, 'count': battlesList.length});
        })
        .catch(function (errObject) {
            console.log('In catch block --> API - ', req.originalUrl);
            console.log(errObject);
			console.log('\n-------------------');
			return res.send({'error': 1, 'msg': 'Battle list not found. Something wen\'t wrong. Please try again later.', 'data': [], 'count': 0});
        });
});// EOF api

//
// Get some stats
router.get('/stats', function (req, res, next) {
    console.log('\n---------------------');
    console.log('API Call Start --> ', req.originalUrl);

    // connection
    let mysqlConnection = req.mysqlConnection;

    // queries
    let mysqlQueryAttacker = 'SELECT attacker_king, count(attacker_king) as cnt FROM '+TABLE_NAME+' WHERE attacker_king != "" GROUP BY attacker_king ORDER BY cnt DESC LIMIT 1';
    let mysqlQueryDefender = 'SELECT defender_king, count(defender_king) as cnt FROM '+TABLE_NAME+' WHERE defender_king != "" GROUP BY defender_king ORDER BY cnt DESC LIMIT 1';
    let mysqlQueryRegion = 'SELECT region, count(region) as cnt FROM '+TABLE_NAME+' WHERE region != "" GROUP BY region ORDER BY cnt DESC LIMIT 1';
    let mysqlQueryName = 'SELECT name, count(name) as cnt FROM '+TABLE_NAME+' WHERE name != "" GROUP BY name ORDER BY cnt DESC LIMIT 1';
    let mysqlQueryOutcome = 'SELECT attacker_outcome, count(attacker_outcome) as cnt FROM '+TABLE_NAME+' WHERE attacker_outcome != "" GROUP BY attacker_outcome';
    let mysqlQueryBattleType = 'SELECT DISTINCT(battle_type) FROM '+TABLE_NAME+' WHERE battle_type != ""';
    let mysqlQueryDefenderSize = 'SELECT MIN(defender_size) as min, MAX(defender_size) as max, AVG(defender_size) as avg FROM '+TABLE_NAME+' WHERE defender_size > 0';

    // stats object
    let stats = {
        'most_active': {
            'attacker_king': '',
            'defender_king': '',
            'region': '',
            'name': ''
        },
        'attacker_outcome': {
            'win': 0,
            'loss': 0
        },
        'battle_type': [],
        'defender_size': {
            'average': 0,
            'min': 0,
            'max': 0
        }
    };

    // Query most active attacker king
    mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryAttacker)
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['most_active']['attacker_king'] = queryResult[0]['attacker_king'];
            }

            // Query most active defender king
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryDefender);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['most_active']['defender_king'] = queryResult[0]['defender_king'];
            }

            // Query most active region
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryRegion);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['most_active']['region'] = queryResult[0]['region'];
            }

            // Query most active name
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryName);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['most_active']['name'] = queryResult[0]['name'];
            }

            // Query attacker outcome
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryOutcome);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                for(let i = 0; i < queryResult.length; i++) {
                    if(queryResult[i]['attacker_outcome'] === 'win') {
                        stats['attacker_outcome']['win'] = queryResult[i]['cnt'];
                    }
                    else if(queryResult[i]['attacker_outcome'] === 'loss') {
                        stats['attacker_outcome']['loss'] = queryResult[i]['cnt'];
                    }
                }                
            }

            // Query unique battle types
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryBattleType);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['battle_type'] = queryResult.map(function(i) {return i['battle_type'];});
            }

            // Query defender size
            return mysqlFnUtil.poolQuery(mysqlConnection, mysqlQueryDefenderSize);
        })
        .then(function (queryResult) {
            // Success
            if(queryResult && queryResult.length > 0) {
                stats['defender_size']['average'] = queryResult[0]['avg'].toFixed(2);
                stats['defender_size']['min'] = queryResult[0]['min'];
                stats['defender_size']['max'] = queryResult[0]['max'];
            }

            // send response
            return res.send({'error': 0, 'msg': 'Stats', 'data': stats});
        })
        .catch(function (errObject) {
            console.log('In catch block --> API - ', req.originalUrl);
            console.log(errObject);
            console.log('\n-------------------');
            return res.send({'error': 1, 'msg': 'Stats not found. Something wen\'t wrong. Please try again later.', 'data': {}});
        });
});// EOF api

module.exports = router;