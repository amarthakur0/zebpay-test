import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
	// Required vars to store data
	battlePlaces: Array<string>;
	battleCount: number = 0;
	battleStats;
	searchResult: any;

  constructor(private _http: Http) { }

  // Get list of all the places where battle has taken place
  getBattlePlaces() {
    return this._http.get("/list")
      .map(result => this.battlePlaces = result.json().data);
  }

  // Get total number of battle occurred
  getBattleCount() {
    return this._http.get("/count")
      .map(result => this.battleCount = result.json().count);
  }

  // Get battle stats
  getBattleStats() {
    return this._http.get("/stats")
      .map(result => this.battleStats = result.json().data);
  }

  // Get battle stats
  searchBattle(queryStr) {
    return this._http.get("/search?"+queryStr)
      .map(result => this.searchResult = result.json().data);
  }
}
