import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  // Required vars to store data
	battlePlaces: Array<string>;
  battleCount: number = 0;
  battleStats;

	// Instance of the DataService
  constructor(private _dataService: DataService) {

    // Get list of all the places where battle has taken place
    this._dataService.getBattlePlaces()
        .subscribe(res => this.battlePlaces = res);

    // Get total number of battle occurred
    this._dataService.getBattleCount()
        .subscribe(res => this.battleCount = res);

    // Get battle stats
    this._dataService.getBattleStats()
        .subscribe(res => this.battleStats = res);
  }

  ngOnInit() {
  }

}
