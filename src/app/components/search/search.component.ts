import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
	// Required vars to store data
	searchResult: any;

  constructor(private _dataService: DataService) {  	
  }

  ngOnInit() {
  }

  // search
	searchBattle(f: NgForm) {
		let formValueObj = f.value;
		let serachQueryStr = '';
		for(let i in formValueObj) {
			if(formValueObj[i]) {
				serachQueryStr += i+'='+formValueObj[i]+'&';
			}
		}

		console.log(serachQueryStr);

		// all empty
		if(!serachQueryStr) {
			alert('All fields empty');
			return false;
		}

		// Search now
    this._dataService.searchBattle(serachQueryStr)
        .subscribe(res => this.searchResult = res);

    return false;
  }
}
